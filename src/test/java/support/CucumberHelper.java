package support;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;

public class CucumberHelper {

    @After()
    public void teraDown(){
        WebDriverRunner.getWebDriver().quit();
    }

    @AfterStep
    public void screen(){
        Selenide.screenshot(System.currentTimeMillis() + "test");
    }
}
