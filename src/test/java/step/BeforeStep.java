package step;

import com.codeborne.selenide.Selenide;
import io.cucumber.java.en.Given;

public class BeforeStep {
    @Given("Я нахожусь на странице {string}")
    public void on_page(String arg0) {
        Selenide.open(arg0);
    }
}
