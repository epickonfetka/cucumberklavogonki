package step;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.$x;

public class KlavogonkiStep {
    private final SelenideElement startGameButton = $x("//a[text()='Начать игру']");
    private final SelenideElement inputField = $x("//input[@type='text']");
    private final SelenideElement highlightWord = $x("//span[@class='highlight']");
    private final SelenideElement afterFocus = $x("//span[@id='afterfocus']");
    private final SelenideElement closeWindowButton = $x("//input[@value='Закрыть']");

    static{
        Configuration.timeout = 60000;
    }

    private String getCurrentWord(){
        return highlightWord.getText().replaceAll("c", "с").replaceAll("o", "о");
    }

    @When("Начинаем игру")
    public void startGame(){
        closeWindowButton.click();
        if(startGameButton.isDisplayed()){
            startGameButton.click();
        }
    }

    @And("Ждем пока игра начнется 30 секунд")
    public void isGameStared(){
        highlightWord.click();
    }

    @And("Вводим подсвеченное слово в цикле")
    public void playGame(){
        while(true){
            String tempText = getCurrentWord();
            String focusSymbol = afterFocus.getText();
            inputField.sendKeys(tempText);
            if(focusSymbol.equals(".")){
                inputField.sendKeys(".");
                break;
            }
            inputField.sendKeys(Keys.SPACE);
        }
    }
    @Then("Фиксируем что игра завершена и символов в минуту больше чем {string}")
    public void gameEnd(String minValue){
        String result = $x("//td[text()='Это вы']//ancestor-or-self::div//div[@class='stats']//div[2]/span/span").getText();
        int minValueNumber = Integer.parseInt(minValue);
        int resultNumber = Integer.parseInt(result);
        System.out.println("Количетсво знаков в минуту: " + resultNumber);
        Assertions.assertTrue(resultNumber>minValueNumber, "Актуальный результат был " + resultNumber);
    }

}
