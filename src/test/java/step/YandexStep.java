package step;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.jupiter.api.Assertions;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class YandexStep {
    @Given("Вводим в поиск словосочетание {string}")
    public void search(String value){
        $x("//input[@id='text']").sendKeys(value);
    }

    @And("Кликаем на кнопке поиска")
    public void clickOnFindBtn(){
        $x("//button[@type='submit']").click();
    }

    @Then("{string} содержится в поисковой выдаче")
    public void checkIfTitleExist(String title){
        ElementsCollection titles = $$x("//li[@class='serp-item serp-item_card']/div/div[1]");
        titles.forEach(x-> System.out.println(x.getText()));

        SelenideElement element = titles
                .stream().filter(x->x.getText().contains(title))
                .findFirst().orElse(null);
        Assertions.assertNotNull(element);
    }

}
